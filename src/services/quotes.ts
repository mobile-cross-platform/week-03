import { Quote } from '../data/quotes.interface';

export class QuotesService {
    private favoriteQuotes: Quote[] = [];

    addQuoteToFavorites(quote: Quote) {
        this.favoriteQuotes.push(quote);
    }

    removeQuoteFromFavorites(quote: Quote) {}
    getFavoriteQuotes() {}
    isFavorite(quote: Quote) {}
}