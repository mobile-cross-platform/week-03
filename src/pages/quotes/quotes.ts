import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quotes } from '../../data/quotes.interface';
import { QuotesService } from '../../services/quotes';

/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit {
  quoteCollection: {category: string, quotes: Quotes[], icon: string} [];
  data: {quotes: Quotes[]};
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private quotesService: QuotesService) {
  }

  ngOnInit() {
    this.quoteCollection=this.navParams.data;
    this.data = this.navParams.data.quotes;
    console.log(this.quoteCollection);
    console.log(this.data);
  }

  onAddQuote(quote: Quotes) {
    this.quotesService.addQuoteToFavorites(quote);
  }

  onShowAlert(data:Quotes) {
    const alert = this.alertCtrl.create({
      title : 'Add Quote',
      subTitle: '',
      message : 'Are you sure you want to add the quote to favorites?',
      buttons: [
        { 
          text: 'OK',
          handler: () =>{
            console.log("OK is clicked.");
            this.onAddQuote(data);
            console.log(this.quotesService);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked.");
          }
        }
      ]
    });
  alert.present();
  }
}
